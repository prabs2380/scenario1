# Scenario 1 Proposed Architecture Improvements

1. **Enhanced Security:** Implement a firewall to manage outbound traffic and configure a route table. This approach negates the need for an Internet Gateway for the EC2 instance, thereby enhancing security.
2. **Secure Communication:** Adopt HTTPS on port 443 instead of HTTP on port 80. Unlike HTTP, HTTPS encrypts the data transmitted and received, ensuring secure communication.
3. **Traffic Redirection:** Redirect all inbound HTTP traffic to HTTPS. This ensures that even if a request is made over HTTP, it will be securely handled over HTTPS.
4. **Domain Name Resolution:** Incorporate Amazon Route 53 to provide a fully qualified domain name (FQDN). This makes the system easily recognizable and more user-friendly.

# screenshot before Uppercase

![Alt text](before.png)

# screenshot after 

![Alt text](after.png)
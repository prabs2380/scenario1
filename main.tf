# Define VPC
data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "fleet_sg" {
  name        = "fleet_sg"
  description = "Allow inbound traffic on ports 80"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_launch_template" "nginx" {
  name_prefix   = "nginx"
  image_id      = "ami-097c4e1feeea169e5"
  instance_type = "t2.micro"

  iam_instance_profile {
    name = aws_iam_instance_profile.s3_access.name
  }
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "Server fleetA"
    }
  }
  user_data = base64encode(<<-EOF
                                #!/bin/bash
                                sudo yum update -y
                                sudo yum install nginx -y
                                sudo aws s3 cp s3://fleet-s3-bucket-00000/ /usr/share/nginx/html/ --recursive
                                sudo systemctl enable nginx
                                sudo systemctl start nginx
                            EOF
  )

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [aws_security_group.fleet_sg.id]

  }
}

resource "aws_subnet" "subnet1" {
  vpc_id            = data.aws_vpc.default.id
  cidr_block        = "172.31.100.0/24"
  availability_zone = "ap-southeast-1a"
}

resource "aws_subnet" "subnet2" {
  vpc_id            = data.aws_vpc.default.id
  cidr_block        = "172.31.150.0/24"
  availability_zone = "ap-southeast-1b"
}

resource "aws_subnet" "subnet3" {
  vpc_id            = data.aws_vpc.default.id
  cidr_block        = "172.31.200.0/24"
  availability_zone = "ap-southeast-1c"
}

resource "aws_autoscaling_group" "asg" {
  desired_capacity  = 3
  max_size          = 3
  min_size          = 1
  health_check_type = "EC2"

  launch_template {
    id      = aws_launch_template.nginx.id
    version = "$Latest"
  }

  vpc_zone_identifier = [aws_subnet.subnet1.id, aws_subnet.subnet2.id, aws_subnet.subnet3.id]
  target_group_arns   = [aws_lb_target_group.alb_tg.arn]

}

resource "aws_security_group" "alb_sg" {
  name        = "alb_sg"
  description = "Allow inbound traffic on ports 80 and 443"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_lb" "alb" {
  name               = "fleet-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_subnet.subnet1.id, aws_subnet.subnet2.id, aws_subnet.subnet3.id]
}

resource "aws_lb_target_group" "alb_tg" {
  name     = "fleet-alb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg.arn
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "fleet-s3-bucket-00000"
  acl    = "private"
  tags = {
    Name        = "increment"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.bucket.bucket
  key    = "index.html"
  source = "./index.html"
  acl    = "private"
}


resource "aws_iam_role" "s3_access" {
  name = "s3_access"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "s3_access" {
  name = "s3_access"
  role = aws_iam_role.s3_access.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetObject",
          "s3:ListBucket"
        ]
        Effect = "Allow"
        Resource = ["arn:aws:s3:::fleet-s3-bucket-00000",
        "arn:aws:s3:::fleet-s3-bucket-00000/index.html"]
      },

    ]
  })
}

resource "aws_iam_instance_profile" "s3_access" {
  name = "s3_access"
  role = aws_iam_role.s3_access.name
}
